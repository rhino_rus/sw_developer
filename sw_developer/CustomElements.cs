﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using common_dll;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Media;

namespace sw_developer
{

    public interface IRefreshable
    {
        void Refresh();
    }

    public interface IMenu
    {
        void DeactivateAll();
    }

    public class PanelForCustomControls : StackPanel
    {
        private event RoutedEventHandler OnElementsRemove;

        private RoutedEventHandler
            _editFinishedHandler,
            _clickHandler;

        public PanelForCustomControls(List<Dictionary<ControlParam, string>> lst = null,
            RoutedEventHandler removeHandler = null,
            RoutedEventHandler editFinishedHandler = null,
            RoutedEventHandler clickHandler = null)
        {
            _editFinishedHandler = editFinishedHandler;
            _clickHandler = clickHandler;
            OnElementsRemove = new RoutedEventHandler(removeHandler);

            if (lst != null)
                Fill(lst);
        }

        public void Add(KeyValuePair<int, string> elem)
        {
            CustomControlElement el = new CustomControlElement(_clickHandler, _editFinishedHandler, this);
            //el.DisplayName = elem.Value;
            el.Name = elem.Value;
            el.ID = elem.Key;
            this.Children.Add(el);
        }

        public void RemoveAt(int position)
        {
            //с формы не удаляю, будет refresh
            List<CustomControlElement> lst = new List<CustomControlElement>();
            lst.Add(this.Children[position] as CustomControlElement);
            OnElementsRemove(lst, null);
        }

        public void RemoveCheckedElements()
        {
            //с формы не удаляю, будет refresh
            List<CustomControlElement> lst = new List<CustomControlElement>();

            for (int i = 0; i < this.Children.Count; i++)
            {
                if ((this.Children[i] as CustomControlElement).Checkbox.IsChecked == true)
                    lst.Add(this.Children[i] as CustomControlElement);
            }

            OnElementsRemove(lst, null);
        }

        public List<int> GetCheckedId() // этот метод попросил Димка
        {
            List<int> lst = new List<int>();

            for (int i = 0; i < this.Children.Count; i++)
            {
                if ((this.Children[i] as CustomControlElement).Checkbox.IsChecked == true)
                    lst.Add((this.Children[i] as CustomControlElement).ID);
            }

            return lst;
        }

        public void Fill(List<Dictionary<ControlParam, string>> lst)
        {
            foreach (var elem in lst)
            {
                CustomControlElement e = new CustomControlElement(_clickHandler, _editFinishedHandler);
                foreach (var dict in elem)
                {
                    if (dict.Key == ControlParam.Id)
                    {
                        e.ID = int.Parse(dict.Value);
                    }
                    if (dict.Key == ControlParam.Name)
                    {
                        e.DisplayName = dict.Value;
                    }
                }
                this.Children.Add(e);
            }
        }

        public void Clear()
        {
            this.Children.Clear();
        }

        public void ClearAndFill(List<Dictionary<ControlParam, string>> lst)
        {
            Clear();
            Fill(lst);
        } // по многочисленным просьбам

        public void ClearAndFill(string response)
        {
            var lst = Json.ConvertToObject<List<Dictionary<ControlParam, string>>>(response);
            Clear();
            Fill(lst);
        }

        public void SortStepUp()
        {
            List<CustomControlElement> lst = new List<CustomControlElement>();
            foreach (var elem in this.Children)
                lst.Add(elem as CustomControlElement);

            lst.Sort(UpSort);

            this.Children.Clear();

            foreach (var elem in lst)
                this.Children.Add(elem);
        }

        public void SortStepDown()
        {
            List<CustomControlElement> lst = new List<CustomControlElement>();
            foreach (var elem in this.Children)
                lst.Add(elem as CustomControlElement);

            lst.Sort(DownSort);

            this.Children.Clear();

            foreach (var elem in lst)
                this.Children.Add(elem);
        }

        private int UpSort(object o1, object o2)
        {
            return (o1 as CustomControlElement).DisplayName.CompareTo((o2 as CustomControlElement).DisplayName);
        }

        private int DownSort(object o1, object o2)
        {
            return (o2 as CustomControlElement).DisplayName.CompareTo((o1 as CustomControlElement).DisplayName);
        }

    }

    public class CustomControlElement : Grid
    {
        private string _displayName;

        public int ID { get; set; }

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                _displayName = value;
                ClickableField.Content = value;
            }
        }

        public object BindedObject { get; set; }


        public bool inEditMode = false;

        private RoutedEventHandler Handler;
        public event RoutedEventHandler OnChangeFinished;

        public CheckBox Checkbox;
        private ButtonWithParentLink ClickableField;
        private TextBox EditingTextBox;
        private Button EditButton;

        public CustomControlElement(RoutedEventHandler clickHandler, RoutedEventHandler changeFinisedHandler, object obj = null)
        {
            Handler = clickHandler;
            OnChangeFinished += changeFinisedHandler;
            BindedObject = obj;

            Init();
        }

        private void Init()
        {
            this.Margin = new Thickness(20, 5, 20, 0);
            EditingTextBox = new TextBox { MinWidth = 150, MaxLength = 50 };


            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(30) });
            this.ColumnDefinitions.Add(new ColumnDefinition());
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });

            InitalizeChildren();
            AddAllChildren();

            DisplayName = "Безымянный";
        } // Инициализация неизменяющихся стилей

        private void InitalizeChildren()
        {
            Checkbox = new CheckBox
            {
                Style = (Style)this.FindResource("CustomCheckBox"),
                VerticalAlignment = VerticalAlignment.Center
            };

            ClickableField = new ButtonWithParentLink
            {
                Parent = this,
                Template = (ControlTemplate)this.FindResource("ButtonTemplate"),
                Style = (Style)this.FindResource("CustomElementStyle"),
            };

            if (inEditMode == true)
            {
                EditingTextBox.Text = DisplayName;
                ClickableField.Content = EditingTextBox;
                EditButton = GetButtonBySource("../images/confirm.png");
            }
            else
            {
                ClickableField.Content = DisplayName;
                EditButton = GetButtonBySource("../images/edit.png");
            }

            Checkbox.SetValue(Grid.ColumnProperty, 0);
            ClickableField.SetValue(Grid.ColumnProperty, 1);
            EditButton.SetValue(Grid.ColumnProperty, 2);

            Checkbox.Click += Checkbox_Click;
            EditButton.Click += EditButton_Click;
        } // Инициализация стилей дочерних элементов. (Стили подменяются)

        private void AddAllChildren()
        {
            this.Children.Add(Checkbox);
            this.Children.Add(ClickableField);
            this.Children.Add(EditButton);

            ClickableField.Click += Handler;
        }

        void Checkbox_Click(object sender, RoutedEventArgs e)
        {
            if (inEditMode) // обрабатываем случай, когда мы находимся в режиме редактирования. Чекбокс не установится, а Edit закроется.
            {
                inEditMode = false;
                this.Children.Clear();
                InitalizeChildren();
                AddAllChildren();
            }
            if (Checkbox.IsChecked == true)
            {
                ClickableField.Background = Brushes.LightGreen;
            }
            else
            {
                ClickableField.Background = new SolidColorBrush(Color.FromRgb(217, 235, 246));
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (inEditMode)
            {
                DisplayName = EditingTextBox.Text; // Сохраняем то, что отредактировали
                OnChangeFinished(this, null);
                inEditMode = false;
            }
            else
            {
                inEditMode = true;
            }

            this.Children.Clear();
            InitalizeChildren();
            AddAllChildren();
        }

        Button GetButtonBySource(string source)
        {
            return new Button
            {
                Width = 30,
                Height = 30,
                Background = Brushes.White,
                Content = new Image { Height = 20, Width = 20, Source = new BitmapImage(new Uri(source, UriKind.Relative)) }
            };
        }  // этот метод желательно перенести.

        /// <summary>
        /// Служит для упрощенного получения идентификаторов элемента
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ID.ToString();
        }
    }

    public class MenuItem : Button
    {

        private Grid grid;
        private Image img;
        private string _imgSource;
        private TextBlock textBlock;
        private string _itemName;
        private bool isRoll = false;

        public MenuItem(string imageSource, string itemName)
        {
            _imgSource = imageSource;
            _itemName = itemName;

            Init();
        }

        private void Init()
        {
            this.Height = 50;
            this.Width = 200;
            this.Margin = new Thickness(0, -1, 0, -1);
            this.Template = (ControlTemplate)this.FindResource("MenuButton");
            Deactivate(); // settin bg color

            grid = new Grid();
            this.Content = grid;
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(150) });

            InitChildren();

            grid.Children.Add(img);
            grid.Children.Add(textBlock);
        }

        private void InitChildren()
        {
            this.img = new Image
            {
                Height = 35,
                Width = 35,
                Source = new BitmapImage(new Uri(_imgSource, UriKind.Relative))
            };

            this.textBlock = new TextBlock
            {
                Text = _itemName,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                FontWeight = FontWeights.Bold,
                MaxWidth = 135,
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center
            };
            Grid.SetColumn(img, 0);
            Grid.SetColumn(textBlock, 1);
        }

        public void TurnRoll()
        {
            DoubleAnimation buttonAnimation = new DoubleAnimation();
            buttonAnimation.From = this.ActualWidth;
            buttonAnimation.Duration = TimeSpan.FromMilliseconds(300);

            if (isRoll) //если уже свёрнуто
            {
                buttonAnimation.To = 200;
                isRoll = false;
            }
            else
            {
                buttonAnimation.To = 50;
                isRoll = true;
            }


            this.BeginAnimation(Button.WidthProperty, buttonAnimation);
        }

        public void Activate()
        {
            Color c = Color.FromRgb(174, 211, 235);
            this.Background = new SolidColorBrush(c); // 196, 223, 240
        }

        public void Deactivate()
        {
            Color c = Color.FromRgb(217, 235, 246);
            this.Background = new SolidColorBrush(c);
        }

    }

    public class ButtonWithParentLink : Button
    {
        public new CustomControlElement Parent { get; set; }

        public ButtonWithParentLink(CustomControlElement parent = null)
        {
            Parent = parent;
        }
    } // обработчик привязывается к этой кнопке. Прикол в том, что в качестве параметра в обработчик лучше передать весь объект, для этого сделал такой класс.

    public class DayUnit : Button
    {
        private TextBlock numberDay;
        private TextBlock stringDay;
        private TextBlock taskBlock;
        private DateTime _date;

        public DateTime Day
        {
            get
            {
                return _date;
            }
            private set
            {
                _date = value;
            }
        }

        public string NumDay
        {
            get
            {
                switch (_date.ToString("dddd"))
                {
                    case "понедельник":
                        return "1";
                    case "вторник":
                        return "2";
                    case "среда":
                        return "3";
                    case "четверг":
                        return "4";
                    case "пятница":
                        return "5";
                    case "суббота":
                        return "6";
                    default:
                        throw new Exception("День недели не лежит в диапазоне понедельник - суббота.");
                }
            }
            private set
            {
                throw new MemberAccessException("Поле недоступно для редактирования.");
            }
        }

        public DayUnit(DateTime date, bool hasTask = false)
        {
            _date = date;

            Init();

            if (hasTask == false)
                taskBlock.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Init()
        {
            this.Template = (ControlTemplate)this.FindResource("MenuButton");
            Deactivate(); //set default color;

            InitChildren();
        }

        private void InitChildren()
        {
            StackPanel sPanel = new StackPanel { Width = 200 };
            this.Content = sPanel;
            this.Margin = new Thickness(-1, -1, 0, 0);
            this.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });

            numberDay = new TextBlock
            {
                FontSize = 24,
                FontWeight = FontWeights.Bold,
                Text = _date.Day.ToString(),
                Margin = new Thickness(15, 0, 0, 0),
                TextAlignment = TextAlignment.Left
            };

            stringDay = new TextBlock
            {
                FontSize = 12,
                Text = _date.ToString("dddd"),
                Margin = new Thickness(15, 0, 0, 5),
                TextAlignment = TextAlignment.Left
            };

            taskBlock = new TextBlock
            {
                FontSize = 12,
                Foreground = Brushes.Green,
                TextAlignment = System.Windows.TextAlignment.Center,
                Margin = new Thickness(10, 10, 0, 0),
                Text = "Есть задание"
            };

            grid.Children.Add(numberDay);
            grid.Children.Add(taskBlock);

            Grid.SetColumn(taskBlock, 1);

            sPanel.Children.Add(grid);
            sPanel.Children.Add(stringDay);

        }

        public void Activate()
        {
            Color c = Color.FromRgb(174, 211, 235);
            this.Background = new SolidColorBrush(c); // 196, 223, 240
        }

        public void Deactivate()
        {
            Color c = Color.FromRgb(217, 235, 246);
            this.Background = new SolidColorBrush(c);
        }
    }

    public static class PasswordGenerator
    {
        public static string GetPassword()
        {
            string PassChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@_-.0123456789";
            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < 8; i++)
            {
                sb.Append(PassChars[rand.Next(PassChars.Length - 1)]);
            }
            return sb.ToString();
        }
    } // супер 228 - надёжный алгоритм генерации нового пароля.
}
