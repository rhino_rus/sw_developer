﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using common_dll;
using sw_developer.SwallowService;
using sw_developer.LocalService;
using sw_developer.Swallow;
using System.Windows;

namespace sw_developer.Client
{
    public static class Execute
    {
        public static Response ExecuteMethod(Request request)
        {
            Response response = new Response();

            if (Options.IsRealese)
            {
                var client = new SwallowService.ServiceClient();
                response = client.ExecuteMethod(request);
            }
            else
            {
                var client = new LocalService.ServiceClient();
                response = client.ExecuteMethod(request);
            }

            if (response.IsError)
            {
                MessageBox.Show(response.Message, "Ошибка");
            }

            return response;
        }
    }
}
