﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sw_developer
{
    /// <summary>
    /// Логика взаимодействия для EditAdmin.xaml
    /// </summary>
    public partial class EditAdmin : Page
    {
        MainWindow mainWindow;
        int ID = -1;
        string _name = string.Empty;
        string _login = string.Empty;
        string _birthdate = string.Empty;

        public EditAdmin(MainWindow window, int id)
        {
            InitializeComponent();
            mainWindow = window;

            ID = id;
        }

        private void RefreshCard()
        {
           
        }

        private void btnChangePass_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.mainFrame.Navigate(new Admins(mainWindow)); // возврат после удаления

        }

        private void btnAcceptChanges_Click(object sender, RoutedEventArgs e)
        {
           
        }
    }
}
