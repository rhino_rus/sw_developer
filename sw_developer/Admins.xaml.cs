﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sw_developer
{
    /// <summary>
    /// Логика взаимодействия для Admins.xaml
    /// </summary>
    public partial class Admins : Page
    {
        PanelForCustomControls table;
        MainWindow mainWindow;

        public Admins(MainWindow window)
        {
            InitializeComponent();
            mainWindow = window;

            table = new PanelForCustomControls(
                null,
                RemoveElementsHandler,
                EditingFinishedHandler,
                ElementClickHandler);

            ScrollBox.Content = table;

            //Посмотри, какие методы у PanelForCustomControls есть, чтобы всё вспомнить.
        }

        private void btnRemoveSelectedElements_Click(object sender, RoutedEventArgs e)
        {
            table.RemoveCheckedElements();
        }

        private void btnAddElement_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RemoveElementsHandler(object sender, RoutedEventArgs e)
        {

        }

        private void EditingFinishedHandler(object sender, RoutedEventArgs e)
        {

        }

        private void ElementClickHandler(object sender, RoutedEventArgs e)
        {
            mainWindow.mainFrame.Navigate(new Admins(mainWindow));
        }
    }
}
