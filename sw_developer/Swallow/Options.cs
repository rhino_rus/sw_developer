﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sw_developer.Swallow
{
    public static class Options
    {
        public static bool IsRealese = false;

        private static int SessionID { get; set; }


        public static void SetSessionID(int sessionID)
        {
            SessionID = sessionID;
        }


        public static int GetSessionID()
        {
            if (SessionID == 0)
                throw new Exception("Идентификатор сессии не заполнен");

            return SessionID;
        }
    }
}
